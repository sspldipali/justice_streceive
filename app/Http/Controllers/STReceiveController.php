<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ReceiptModel;
use App\Models\STReceiveModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Zipper;

class STReceiveController extends Controller
{
	private $controllerModel = null;

	public function __construct()
	{
		$this->controllerModel = new STReceiveModel();
	}

	public function checkService(Request $request)
	{
		return response()->json(array("message" => "Service is working", "uploadFilePath" => $this->getFileUploadPath()), 200);
	}

	public function getFileUploadPath()
	{
		return env("FILE_UPLOAD_PATH");
	}

	public function checkZipExtract(Request $request)
	{
		Zipper::make('F:/files_to_upload/files_uploaded_streceive/12562.zip')->extractTo('F:/files_to_upload/files_uploaded_streceive');
	}

	public function stReceive(Request $request)
	{
		try {
			Log::info("Reached stReceive");
			$requestData = $request->all();
			//Log::info("requestData" . json_encode($requestData));
			$responseJson = array();
			$responseData = array();
			/*foreach ($requestData as $arrKey => $subArrStr) {
				$subArr = json_decode($subArrStr);*/

			foreach ($requestData as $arrKey => $subArrStr) {
				$subArr = json_decode($subArrStr);
				$rData = array();
				//Log::info("Key => " . $key);
				if ($request->hasFile($arrKey)) {
					$file = $request->file($arrKey);
					/* //To save file to the storage/app folder
						                        $file->store($key . ".txt");
					*/
					Log::info("File name: " . $file->getClientOriginalName());

					//To save file to the drive (Another folder other than project folder)
					//$filePath = 'E:/files_to_upload/files_uploaded_streceive/';
					//$filePath = env("FILE_UPLOAD_PATH", "/home/administrator/file_uploads");
					$filePath = 'E:/var/www/html/files_to_upload/';
					$fileName = $file->getClientOriginalName();
					$zipFileName = "";
					if (strpos($fileName, ".zip")) {
						$zipFileName = substr($fileName, 0, -4) . "_" . Carbon::now()->timestamp . ".zip";
					} else {
						$zipFileName = $fileName;
					}
					$file->move($filePath, $zipFileName);

					Log::info("Changing permission of uploaded file...");
					$fileToPerChange = $filePath . "/" . $zipFileName;
					Log::info("Filename to change permission: " . $fileToPerChange);
					chmod($fileToPerChange, 0777);

					$rData['letterFilePath'] = $filePath . $zipFileName;

					/**
					 * Check if the file is a zip file.
					 * You can check this by checking if the fileName has .zip extension.
					 * If the file is a zip file then extract it
					 */
					Log::info("Checking if file has zip extension...");
					Log::info("strpos(fileName, '.zip'): " . strpos($zipFileName, ".zip"));
					if (strpos($zipFileName, ".zip")) {
						//Unzip the uploaded zip file
						$path = $filePath . "/" . $zipFileName;
						$zipperObj = Zipper::make($path);
						Log::info("Get list of files in the zip ");
						$fileList = $zipperObj->listFiles();
						//Log::info("File List: " . json_encode($fileList));
						Log::info("Number of files in zip: " . sizeof($fileList));
						$rData["FileCount"] = sizeOf($fileList);

						Log::info("Unzipping the zip file...");
						/*$path = $filePath . "/" . $fileName;
						Zipper::make($path)->extractTo($filePath);*/
						/*$newFolderPath = $filePath . "/" . substr($zipFileName, 0, -4); // . "_" . Carbon::now('UTC')->timestamp;
							mkdir($newFolderPath);
							Log::info("Changing permission of newly created folder...");
							chmod($newFolderPath, 0777);
							$zipperObj->extractTo($newFolderPath);

							Log::info("Changing permission of all files in newly created folder...");
							Log::info("REached till fileList");
							foreach ($fileList as $fileNm) {
								chmod($newFolderPath . "/" . $fileNm, 0777);
						*/
						$zipperObj->close();
					}
				} else {
					/*foreach ($subArr as $key => $value) {
						$rData[$key] = $value;
					}*/
				}
				array_push($responseData, $rData);
			}
			//Save data into streceive table
			//$stReceiveModel = $this->controllerModel->saveSTReceiveData($requestData);
			$stReceiveModelArr = $this->controllerModel->saveSTReceiveData($requestData, $request);
			$receiptModel = new ReceiptModel();
			$receiptModelArr = $receiptModel->saveReceiptRecords(json_decode($requestData['receiptList']), $request, $requestData['No']);

			$stReceiveModelIdArr = array();

			$returnStReceiveModels = array();
			$returnReceiptModels = array();
			$savedSTReceiveRecordsId = array();
			foreach ($stReceiveModelArr as $stReceiveModel) {
				//Log::info("Saved record id: " . $stReceiveModel->id);
				array_push($savedSTReceiveRecordsId, $stReceiveModel->id);
				array_push($returnStReceiveModels, $stReceiveModel->id);
			}
			Log::info("STReceive Saved Ids: " . json_encode($savedSTReceiveRecordsId));

			$savedSTReceiptRecordsId = array();
			foreach ($receiptModelArr as $receiptModel) {
				//Log::info("Saved record id: " . $receiptModel->id);
				array_push($savedSTReceiptRecordsId, $receiptModel->id);
				array_push($returnReceiptModels, $receiptModel->id);
			}
			Log::info("STReceipt Saved Ids: " . json_encode($savedSTReceiptRecordsId));
			$stReceiveModelIdArr['stReceiveIds'] = $returnStReceiveModels;
			$stReceiveModelIdArr['receiptIds'] = $returnReceiptModels;

			$responseJson['message'] = "Report submitted successfully!";
			$responseJson['success'] = true;
			$responseJson['data'] = array(
				"streceiveData" => $responseData,
				"stReceiveId" => $stReceiveModelIdArr,
			);
			//Log::info(json_encode($request->allFiles()));
			return response()->json($responseJson, 200, array("Content-Type" => "application/json", "Accept" => "application/json"));
		} catch (\Exception $e) {
			Log::info("Exception: " . $e->getMessage());
			Log::error($e);

			$exceptionJson = array(
				'message' => 'Something went wrong!',
				'success' => false,
				'exception' => $e->getMessage(),
			);
			return response()->json(
				$exceptionJson,
				400,
				array("Content-Type" => "application/json", "Accept" => "application/json")
			);
		}
	}
}
