<?php
namespace App\Models;

use App\Models\DAppModel;

class ReceiptModel extends DAppModel {
	protected $table = "receipt";

	protected $fillable = [
		"id", "Year", "Qtr", "No", "Court", "BookNo", "BillNo", "Paid", "Receipt",
	];

	public function __construct() {
		parent::__construct();
	}

	public function saveReceiptRecords($receiptRecords, $request, $No) {
		$receiptModelArr = array();
		foreach ($receiptRecords as $receiptRecord) {
			$receiptModel = new ReceiptModel();
			$receiptModel->Year = $receiptRecord->FiscalYear;
			$receiptModel->Qtr = $receiptRecord->FiscalQtr;
			$receiptModel->No = $No;
			$receiptModel->Court = $receiptRecord->Court;
			$receiptModel->BookNo = $receiptRecord->BookNo;
			$receiptModel->BillNo = $receiptRecord->BillNo;

			$receiptModel->Paid = $receiptRecord->Paid;
			$receiptModel->Receipt = base64_decode($receiptRecord->Receipt);

			$receiptModel->save();
			array_push($receiptModelArr, $receiptModel);
		}
		return $receiptModelArr;
	}
}
