<?php

namespace App\Models;

use App\Models\DAppModel;
use DB;

class STReceiveModel extends DAppModel {
	protected $table = "streceive";

	protected $fillable = [
		"id", "Type", "Year", "Quarter", "No", "LetterNo", "LetterDate", "Debtor", "BlackNumber",
		"RedNumber", "Court", "Debt", "Result", "PetitionDate", "OrderDate", "Rate", "Paid",
		"Outstanding", "InsureDate", "Insurer", "IDCardNumber", "Accused", "Amount",
		
	];

	public function __construct() {
		parent::__construct();
	}

	public function saveSTReceiveData($requestData, $request) {

		$submissionRecords = json_decode($requestData['submissionRecords']);

		$savedModelArr = array();
		foreach ($submissionRecords as $submissionRecordArr) {
			$stReceiveModel = new STReceiveModel();
			$stReceiveModel->Type = $submissionRecordArr->Type;
			$stReceiveModel->Year = $submissionRecordArr->Year;
			$stReceiveModel->Quarter = $submissionRecordArr->Quarter;
			$stReceiveModel->No = $submissionRecordArr->No;
			$stReceiveModel->LetterNo = $submissionRecordArr->LetterNo;
			if ($submissionRecordArr->LetterDate != "" && $submissionRecordArr->LetterDate != 'NULL') {
				$stReceiveModel->LetterDate = $submissionRecordArr->LetterDate;
			}

			$stReceiveModel->Debtor = $submissionRecordArr->Debtor;
			$stReceiveModel->BlackNumber = $submissionRecordArr->BlackNumber;
			$stReceiveModel->RedNumber = $submissionRecordArr->RedNumber;
			$stReceiveModel->Court = $submissionRecordArr->Court;
			$stReceiveModel->Debt = $submissionRecordArr->Debt;
			$stReceiveModel->Result = $submissionRecordArr->Result;
			if ($submissionRecordArr->PetitionDate != "" && $submissionRecordArr->PetitionDate != 'NULL') {
				$stReceiveModel->PetitionDate = $submissionRecordArr->PetitionDate;
			}

			if ($submissionRecordArr->OrderDate != "" && $submissionRecordArr->OrderDate != 'NULL') {
				$stReceiveModel->OrderDate = $submissionRecordArr->OrderDate;
			}

			$stReceiveModel->Rate = $submissionRecordArr->Rate;
			$stReceiveModel->Paid = $submissionRecordArr->Paid;
			$stReceiveModel->Outstanding = $submissionRecordArr->Outstanding;

			if ($submissionRecordArr->InsureDate != "" && $submissionRecordArr->InsureDate != 'NULL') {
				$stReceiveModel->InsureDate = $submissionRecordArr->InsureDate;
			}

			$stReceiveModel->Insurer = $submissionRecordArr->Insurer;
			$stReceiveModel->IDCardNumber = $submissionRecordArr->IDCardNumber;
			$stReceiveModel->Accused = $submissionRecordArr->Accused;
			$stReceiveModel->Amount = $submissionRecordArr->Amount;
			$stReceiveModel->save();
			array_push($savedModelArr, $stReceiveModel);
		}
		return $savedModelArr;
	}

	/*public function saveSTReceiveData($requestData, $request) {
		$savedModelArr = array();
		foreach ($requestData as $arrKey => $sArrStr) {
			//$sArr = json_decode(json_decode(json_encode($sArrStr)));
			if (!$request->hasFile($arrKey)) {
				//Log::info("InhasFileCondition...");
				$sArr = json_decode(json_decode(json_encode($sArrStr)));
				$stReceiveModel = new STReceiveModel();
				$stReceiveModel->Type = $sArr->Type;
				$stReceiveModel->Year = $sArr->Year;
				$stReceiveModel->Quarter = $sArr->Quarter;
				$stReceiveModel->No = $sArr->No;
				$stReceiveModel->LetterNo = $sArr->LetterNo;
				if ($sArr->LetterDate != "" && $sArr->LetterDate != 'NULL') {
					$stReceiveModel->LetterDate = $sArr->LetterDate;
				}

				$stReceiveModel->Debtor = $sArr->Debtor;
				$stReceiveModel->BlackNumber = $sArr->BlackNumber;
				$stReceiveModel->RedNumber = $sArr->RedNumber;
				$stReceiveModel->Court = $sArr->Court;
				$stReceiveModel->Debt = $sArr->Debt;
				$stReceiveModel->Result = $sArr->Result;
				if ($sArr->PetitionDate != "" && $sArr->PetitionDate != 'NULL') {
					$stReceiveModel->PetitionDate = $sArr->PetitionDate;
				}

				if ($sArr->OrderDate != "" && $sArr->OrderDate != 'NULL') {
					$stReceiveModel->OrderDate = $sArr->OrderDate;
				}

				$stReceiveModel->Rate = $sArr->Rate;
				$stReceiveModel->Paid = $sArr->Paid;
				$stReceiveModel->Outstanding = $sArr->Outstanding;

				if ($sArr->InsureDate != "" && $sArr->InsureDate != 'NULL') {
					$stReceiveModel->InsureDate = $sArr->InsureDate;
				}

				$stReceiveModel->Insurer = $sArr->Insurer;
				$stReceiveModel->IDCardNumber = $sArr->IDCardNumber;
				$stReceiveModel->Accused = $sArr->Accused;
				$stReceiveModel->Amount = $sArr->Amount;
				$stReceiveModel->save();
				array_push($savedModelArr, $stReceiveModel);
				//return $stReceiveModel;
			}

		}
		return $savedModelArr;
	}*/

	public function getSavedSTReceiveData($year, $quarter) {
		$stReceiveData = DB::table($this->table)
			->where('Year', $year)
			->where('Quarter', $quarter)
			->get();
		return $stReceiveData;
	}
}
